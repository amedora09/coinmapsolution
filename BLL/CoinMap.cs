﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CoinMap
    {
        public int Id { get; set; }
        public float Lat { get; set; } 
        public float Lon { get; set; }
        public string Category { get; set; }
        public string CoinMapName { get; set; }
        public string CreatedOn { get; set; }
        public string GeolocationDegrees { get; set; }
    }
}
