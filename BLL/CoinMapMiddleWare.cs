﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CoinMapMiddleWare
    {
        public IEnumerable<venues> venues { get; set; }
        public int totalRec { get; set; }
        public int totalPages { get; set; }
        public int page { get; set; }
    }
    public class venues
    {
            public int id { get; set; }
            public float lat { get; set; }
            public float lon { get; set; }
            public string category { get; set; }
            public string name { get; set; }
            public string created_on { get; set; }
            public string geolocation_degrees { get; set; }
    }
}
