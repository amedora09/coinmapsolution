﻿using BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoinMapWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoinMapController : ControllerBase
    { 
        private HttpClient _client = null;

        public CoinMapController(HttpClient client)
        {
            _client = client;
        }
        /// <summary>
        /// Gets list of CoinMap
        /// </summary>
        /// <returns>IEnumerable list of venues</returns>
        [HttpGetAttribute]
        
        public async Task<CoinMapMiddleWare> Get(int page=1, int limit = 500)
        {
            try
            {
                var response = await _client.GetAsync("https://coinmap.org/api/v1/venues/");
                if (response.IsSuccessStatusCode)
                {
                    var returnedData = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<CoinMapMiddleWare>(returnedData);
                    int totalRec = data.venues.Count();
                    data.totalRec = totalRec;
                    double pageCount = (double)((decimal)totalRec / Convert.ToDecimal(limit));
                    data.totalPages = (int)Math.Ceiling(pageCount);
                    data.page = page;
                    int offset = (page - 1) * limit + 1;
                     data.venues = data.venues.Skip(offset).Take(limit);
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
            

            return null;
        }
    }
}
