﻿using BLL;
using CoinMapWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoinMapWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _iConfiguration;
        private readonly ILogger<HomeController> _logger;
        private readonly HttpClient _httpClient;

        public HomeController(ILogger<HomeController> logger, IConfiguration iConfiguration, HttpClient httpClient)
        {
            _logger = logger;
            _iConfiguration = iConfiguration;
            _httpClient = httpClient;
        }

        public async Task<IActionResult> Index(int page=1,int limit=500)
        {
            int totalPage = 2;
            CoinMapMiddleWare coinData = new CoinMapMiddleWare();
            var  httpResponse = await _httpClient.GetAsync(_iConfiguration["ApiSettings:ApiEndpoint"]+"?page="+page+"&limit="+limit);
            if (httpResponse.IsSuccessStatusCode)
            {
                string returnedData = await httpResponse.Content.ReadAsStringAsync();
                coinData = JsonConvert.DeserializeObject<CoinMapMiddleWare>(returnedData);
                double pageCount = (double)((decimal)coinData.totalRec / Convert.ToDecimal(limit));
                ViewBag.TotalRec = coinData.totalRec;
                ViewBag.TotalPages = (int)Math.Ceiling(pageCount);
                 totalPage = (int)Math.Ceiling(pageCount);
                ViewBag.CurrentPage = page;
            }
            //return View(coinData.venues.ToPagedList(page, totalPage));
            return View(coinData.venues);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
